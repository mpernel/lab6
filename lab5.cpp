#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"
using namespace std;
// using enum to assign each card a suit
enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

// use a struct to save the card value and suit
typedef struct Card {
  Suit suit;
  int value;
} Card;

string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
int myrandom (int i) { return std::rand()%i;}


int main(int argc, char const *argv[]) {
  // IMPLEMENT as instructed below
  /*This is to seed the random generator */
  srand(unsigned (time(0)));

  /*Create a deck of cards of size 52 (hint this should be an array) and
   *initialize the deck*/
  int i = 0;
  int j = 0;
  Card deckArray[52];

  // I am using nested for loops to access each index in deckArray
  // i reptesents the current enum value and j is card number
  // using variable cardNum to to keep track of the number of the card
  for(i = 0; i < 4; i++){
    int cardNum = 2;
    for(j = 0; j < 13; j++){
      deckArray[(j + (13 * i))].value = cardNum;

      if(i == 0){
        deckArray[(j + (13 * i))].suit = SPADES;
      }
      else if(i == 1){
        deckArray[(j + (13 * i))].suit = HEARTS;
      }
      else if(i == 2){
          deckArray[(j + (13 * i))].suit = DIAMONDS;
      }
      else{
          deckArray[(j + (13 * i))].suit = CLUBS;
      }
      cardNum++;
    }
  }


  /*After the deck is created and initialzed we call random_shuffle() see the
   *notes to determine the parameters to pass in.*/
   // this functin shuffles the deck
   random_shuffle(&deckArray[0], &deckArray[52], myrandom);

   /*Build a hand of 5 cards from the first five cards of the deck created
    *above*/
    // uses the newly shuffled deck to have 5 random cards
    Card handArray[] = {deckArray[0], deckArray[1], deckArray[2],
      deckArray[3], deckArray[4]};

    /*Sort the cards.  Links to how to call this function is in the specs
     *provided*/
     // this function sorts the cards according to their orginal index
    std::sort(&handArray[0], &handArray[5], suit_order);

    // need strings to print the name of face cards and the symbol of the suits
    string faceCard = "something";
    string suit = "something";
    /*Now print the hand below. You will use the functions get_card_name and
     *get_suit_code */
     // loop will iterate 5 times because we draw 5 cards
     // justified right with 10 space for formatting output
     // prints the value of the car or the name then the suit as a symbol
    for(i = 0; i < 5; i++){
      if(handArray[i].value < 11){
        cout << std::right << setw(10) << handArray[i].value << " of ";
      }
      else {
        faceCard = get_card_name(handArray[i]);
        cout << std::right << setw(10) << faceCard << " of ";
      }

      suit = get_suit_code(handArray[i]);
      cout << suit << endl;
    }


  return 0;
}


/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/
// this function returns true then the card is in the right place
// otherwise they switch places
// the enum value is used then the card value to determine the order
bool suit_order(const Card& lhs, const Card& rhs) {
  if(lhs.suit < rhs.suit){
    return true;
  }
  else if(lhs.suit == rhs.suit){
    if (lhs.value < rhs.value){
      return true;
    }
    else {
      return false;
    }
  }
  else {
    return false;
  }

}

// this function allows for the suits to be printed as symbols
string get_suit_code(Card& c) {
  switch (c.suit) {
    case SPADES:    return "\u2660";
    case HEARTS:    return "\u2661";
    case DIAMONDS:  return "\u2662";
    case CLUBS:     return "\u2663";
    default:        return "";
  }
}

// this funtin takes the card value and assigns the name of the face cards
// to the value
string get_card_name(Card& c) {
  string faceCard = "something";

  switch(c.value){
    case 11:
      faceCard = "Jack";
    break;
    case 12:
      faceCard = "Queen";
    break;
    case 13:
      faceCard = "King";
    break;
    case 14:
      faceCard = "Ace";
    break;
    default:
      cout << "Error";
    break;
  }
  return faceCard;
}
